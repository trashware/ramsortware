width = 12;
height = 19;
depth = 3;
wall = 0.05;

cube([width, height, wall]);

cube([wall, height, depth]);
cube([width, wall, depth]);

translate([0, height, 0]) {
    cube([width, wall, depth]);
}
translate([width, 0, 0]) {
    cube([wall, height, depth]);
    }
    
translate([4, 3, 0]) {
cube([wall, 16, 3]);
}

translate([8, 3, 0]) {
cube([wall, 16, 3]);
}

translate([0, 3, 0]) {
    cube([width, wall, depth]);
}

translate([0, 11, 0]) {
    cube([width, wall, depth]);
}