# ramsortware
A RAM sorting hardware, useful to keep track of your memories while doing [trashware](https://wiki.golem.linux.it/Trashware).

![picture of a ramsortware](picture.png)

## License
This work is released under the terms of the WTFPL license.

